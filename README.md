- Description

Timber Tanks is a tank battle simulator project powered by the Unreal Engine 4 that makes use of the core features of the engine and C++.

It was created as a hobby project with the objective to learn about the core concepts and fundamentals of the Unreal Engine 4.
You control a tank in an uneven landscape where other enemy tanks keep chasing you to take you down and mortar towers keep vigilant and waiting for you to get into their firing range.
The game is still an early stage prototype but the enemy tanks/mortars AI is already workingand the first level arena is finished.

- Installation

Since the game is still in an early stage to have it working you need to have the Unreal Engine 4.22 installed and use it to build the project.
You can play directly in the engine or create an executable by, in the editor, going to File > Package Project and select the platform of your choosing.

Please bear in mind that this is an early stage prototype so you may encounter some(or many) bugs.